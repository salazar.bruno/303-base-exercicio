package com.br.investimentos.Api.Investimentos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableEurekaClient
@SpringBootApplication
public class ApiInvestimentosApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiInvestimentosApplication.class, args);
	}

}
